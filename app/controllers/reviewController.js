// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Review Model
const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const createReviewOfCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const body = request.body;
    // console.log(body)
    // { bodyStars: 5, bodyNote: 'Good' }

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    if (!(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    const newReview = {
        _id: mongoose.Types.ObjectId(),
        stars: body.bodyStars,
        note: body.bodyNote
    }

    reviewModel.create(newReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Thêm ID của review mới vào mảng reviews của course đã chọn
        courseModel.findByIdAndUpdate(courseId, {
            $push: {
                reviews: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Review Successfully",
                data: data
            })
        })
    })
}

const getReviewById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    reviewModel.findById(reviewId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail review successfully",
            data: data
        })
    })
}

const getAllReview = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    reviewModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all review successfully",
            data: data
        })
    })
}

const getAllReviewOfCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId)
        .populate("reviews")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all review of course successfully",
                data: data
            })
        })
}

const updateReviewByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    if (body.bodyStars !== undefined && !(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateReview = {}

    if (body.bodyStars !== undefined) {
        updateReview.stars = body.bodyStars
    }

    if (body.bodyNote !== undefined) {
        updateReview.note = body.bodyNote
    }

    reviewModel.findByIdAndUpdate(reviewId, updateReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update reivew successfully",
            data: data
        })
    })
}

const deleteReviewByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        courseModel.findByIdAndUpdate(courseId, {
            $pull: { reviews: reviewId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete reivew successfully"
            })
        })
    })
}

module.exports = {
    getReviewById,
    createReviewOfCourse,
    getAllReview,
    getAllReviewOfCourse,
    updateReviewByID,
    deleteReviewByID
}